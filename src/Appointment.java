/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * take the appointment description and date from constructed object
 * return description and date
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Appointment {
    private String strDescription;
    private String strAppointmentDate;

    /*
    Constructor to set description and date
     */
    public Appointment(String strAppointmentDescription, String strDate){
        strDescription = strAppointmentDescription;
        strAppointmentDate = strDate;
    }

    /*
    check if occurs on always return true as dummy
     */
    public boolean occursOn(int nYear, int nMonth, int nday){
        return true;
    }

    /*
    getter to get appointment description if called
     */
    public String getAppointmentDescription(){
        return strDescription;
    }

    /*
    getter to get appointment date if called
     */
    public String getDate(){
        return strAppointmentDate;
    }

    /*
   take in filename and the appointment object created when user creates an appointment
   and print them to a file,
   the file will include the appointment type, its description, and date
    */
    public static void save(String strFile, Appointment appointmentSaved) throws IOException {
        File fileExisted = new File(strFile);
        FileWriter appendToFile = new FileWriter (fileExisted, true);
        PrintWriter moveAppointmentToFile = new PrintWriter(appendToFile);

        moveAppointmentToFile.println(appointmentSaved.getClass().getSimpleName());
        moveAppointmentToFile.println(appointmentSaved.getAppointmentDescription());
        moveAppointmentToFile.println(appointmentSaved.getDate());
        moveAppointmentToFile.close();

    }

    /*
    Move through the file setting the type, description, and date of each appointment into strings
    if user enters  O, M, or D  then check if type is begins with O,D, or M
    then set description and date for that type of appointment
    into an appointment object of that type and add it
    to an arraylist of appointments return the arraylist
     */
    public static ArrayList<Appointment> load(String strFileName) throws FileNotFoundException {
        ArrayList<Appointment> appointmentsAddedByType = new ArrayList<>();
        File appointmentFile;
        Scanner inputFromFile;
        Scanner scanObject = new Scanner(System.in);
        System.out.print("Enter the type of appointment O = Onetime, D = Daily, or M = Monthly:");
        String strChoiceEntered = scanObject.nextLine();
        try {
            appointmentFile = new File(strFileName);
            inputFromFile = new Scanner(appointmentFile);

            while(inputFromFile.hasNextLine()){
                String strAppointmentType = inputFromFile.nextLine().trim();
                String strAppointmentDescription = inputFromFile.nextLine().trim();
                String strAppointmentDate = inputFromFile.nextLine().trim();

                if( strChoiceEntered.toUpperCase().charAt(0) == 'O'){
                    if(strAppointmentType.toUpperCase().charAt(0) == 'O'){
                    Appointment appOneTime = new Onetime(strAppointmentDescription, strAppointmentDate);
                    appointmentsAddedByType.add(appOneTime);
                    }
                } else if( strChoiceEntered.toUpperCase().charAt(0) == 'M'){
                    if(strAppointmentType.toUpperCase().charAt(0) == 'M'){
                        Appointment appMonthly = new Monthly(strAppointmentDescription, strAppointmentDate);
                        appointmentsAddedByType.add(appMonthly);
                    }
                }else if( strChoiceEntered.toUpperCase().charAt(0) == 'D'){
                    if(strAppointmentType.toUpperCase().charAt(0) == 'D'){
                        Appointment appDaily = new Daily(strAppointmentDescription, strAppointmentDate);
                        appointmentsAddedByType.add(appDaily);
                    }
                }

            }
        } catch(FileNotFoundException f){
            throw new FileNotFoundException("File not found over here");
        }

        return appointmentsAddedByType;

    }


    /*
    Move through the file setting the type, description, and date of each appointment into strings
    if the type starts with O, M, or D then set description and date for that
    type of appointment into an appointment object of that type and add it
    to an arraylist of appointments
    return the arraylist
     */
    public static ArrayList<Appointment> loadAllAppointments(String strFileName) throws FileNotFoundException {
        ArrayList<Appointment> appointmentsAddedFromFile = new ArrayList<>();
        try {
            File appointmentFile = new File(strFileName);
            Scanner inputFromFile = new Scanner(appointmentFile);

            while(inputFromFile.hasNext()){
                String strAppointmentType = inputFromFile.nextLine();
                String strAppointmentDescription = inputFromFile.nextLine().trim();
                String strAppointmentDate = inputFromFile.nextLine().trim();

                if( strAppointmentType.toUpperCase().charAt(0) == 'O'){
                    Appointment appOneTime = new Onetime(strAppointmentDescription, strAppointmentDate);
                    appointmentsAddedFromFile.add(appOneTime);
                } else if( strAppointmentType.toUpperCase().charAt(0) == 'M'){
                    Appointment appMonthly = new Monthly(strAppointmentDescription, strAppointmentDate);
                    appointmentsAddedFromFile.add(appMonthly);
                }else if( strAppointmentType.toUpperCase().charAt(0) == 'D'){
                    Appointment appDaily = new Daily(strAppointmentDescription, strAppointmentDate);
                    appointmentsAddedFromFile.add(appDaily);
                }

            }
            inputFromFile.close();
        } catch(FileNotFoundException f){
            throw new FileNotFoundException("File not found");
        }
        return appointmentsAddedFromFile;

    }

}
