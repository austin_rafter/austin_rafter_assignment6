/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *CS 49J
 *
 *indexOf takes string strFullString and compares with another string strCompare
 * returning the index nStringBegins of the start of the first substring within
 * strFullString that matches to strCompare if there is no match it returns -1
 */

public class E13_9 {
    public static void main(String[] args) {
        String strFullString = "Mississippi";
        String strCompare = "sip";

        System.out.print(indexOf(strFullString,strCompare, 0));

    }


    /*
    Search through strFullString comparing to strCompare
    see if strCompare is part of strFullString
    return the position that the matching section starts at
    from the beginning of strFullString
     */
    public static int indexOf(String strFullString, String strCompare, int nStringBegins){
        /*
        Create character arrays from the string and substring
         */
        char[] chEnteredString = strFullString.toCharArray();
        char[] chEnteredSubString = strCompare.toCharArray();
        int nStringCheck = strFullString.length();
        int nSubCheck = strCompare.length();
        int numMatchingChars = 0;

        /*
        check if the strings are the same
         */
        if(strFullString.equals(strCompare)){
            return nStringBegins;
        }

        /*
        loop up to length of strCompare at each call
        comparing characters in each array.
         */
        for(int i = 0; i < nSubCheck; i++){
            //If characters matching numMatchingChars increases
            if((chEnteredString[i] == chEnteredSubString[i])) {
                numMatchingChars++;
            }
            /*
            if number of matching characters equals the original strCompare length
            then return the distance from the beginning of strFullString that
            the match starts at
             */
            if (numMatchingChars == (strCompare.length() - nStringBegins)){
                return nStringBegins;
            }
        }


        nSubCheck++;
        nStringBegins++;

            /*
            if length of strCompare becomes bigger than length of strFullString
            then there is no matching substring so return -1
             */
            if(nSubCheck == ++nStringCheck) {
                return -1;
            }

            /*
            as long as length of strFullString is greater than or equal to
            length of strCompare, or number of matching characters is not equal
            to the original length of strCompare then recursively call indexOf
            resetting strCompare with an additional space added in front at each call
             */
            if((strFullString.length() >= strCompare.length()) || (numMatchingChars != (strCompare.length() - nStringBegins))) {
                return indexOf(strFullString, " " + strCompare, nStringBegins);
            }
        return nStringBegins;
    }

}
