import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 *Tests occurs on, save, and load
 * on some created objects
 * and pre-determined times
 *
 * Instance of found at
 * https://www.javatpoint.com/downcasting-with-instanceof-operator
 */
import java.io.FileNotFoundException;
import java.io.IOException;

public class P9_5Test  {

    /*
    Create Onetime, monthly, and daily appointment objects
    set the files to check for load and save tests
     */
    private final Appointment oneTimeApp = new Onetime("OneTime test", "2020-10-15");
    private final Appointment dailyApp = new Daily("Daily test", "2020-10-15");
    private final Appointment monthlyApp = new Monthly("Monthly test", "2020-10-15");
    private final String strFileNameOne = "src/addingApps.txt";
    private final String strFileNameTwo = "src/appsAdded.txt";

    /*
    set date for first occurs on test
     */
    String strDateOfAppointmentOne = "2020-10-15";
    String[] splitToIntsOne = strDateOfAppointmentOne.split("-");

    int nYearCompareOne = Integer.parseInt(splitToIntsOne[0]);
    int nMonthCompareOne = Integer.parseInt(splitToIntsOne[1]);
    int nDayCompareOne = Integer.parseInt(splitToIntsOne[2]);

    /*
   set date for second occurs on test
    */
    String strDateOfAppointmentTwo = "2020-11-15";
    String[] splitToIntsTwo = strDateOfAppointmentTwo.split("-");

    int nYearCompareTwo = Integer.parseInt(splitToIntsTwo[0]);
    int nMonthCompareTwo = Integer.parseInt(splitToIntsTwo[1]);
    int nDayCompareTwo = Integer.parseInt(splitToIntsTwo[2]);

    /*
   set date for third occurs on test
    */
    String strDateOfAppointmentThree = "2020-12-05";
    String[] splitToIntsThree = strDateOfAppointmentThree.split("-");

    int nYearCompareThree = Integer.parseInt(splitToIntsThree[0]);
    int nMonthCompareThree = Integer.parseInt(splitToIntsThree[1]);
    int nDayCompareThree = Integer.parseInt(splitToIntsThree[2]);

    /*
   set date for fourth occurs on test
    */
    String strDateOfAppointmentFour = "2020-09-15";
    String[] splitToIntsFour = strDateOfAppointmentFour.split("-");

    int nYearCompareFour = Integer.parseInt(splitToIntsFour[0]);
    int nMonthCompareFour = Integer.parseInt(splitToIntsFour[1]);
    int nDayCompareFour = Integer.parseInt(splitToIntsFour[2]);

    //create constructor for test class
    public P9_5Test(){

    }

    /*
    Check date 2020-10-15
    should return all true
     */
    @Test
    public void testOccursOnTimeOne(){

        assertTrue( oneTimeApp.occursOn(nYearCompareOne,nMonthCompareOne,nDayCompareOne));
        assertTrue(monthlyApp.occursOn(nYearCompareOne,nMonthCompareOne,nDayCompareOne));
        assertTrue(dailyApp.occursOn(nYearCompareOne,nMonthCompareOne,nDayCompareOne));

    }

    /*
    Check date 2020-11-15
    should return false for one time
    and true for monthly and daily
     */
    @Test
    public void testOccursOnTimeTwo(){

        assertFalse(oneTimeApp.occursOn(nYearCompareTwo,nMonthCompareTwo,nDayCompareTwo));
        assertTrue(monthlyApp.occursOn(nYearCompareTwo,nMonthCompareTwo,nDayCompareTwo));
        assertTrue(dailyApp.occursOn(nYearCompareTwo,nMonthCompareTwo,nDayCompareTwo));

    }

    /*
    Check date 2020-12-05
    should only return true for daily
     */
    @Test
    public void testOccursOnTimeThree(){

        assertFalse(oneTimeApp.occursOn(nYearCompareThree,nMonthCompareThree,nDayCompareThree));
        assertFalse(monthlyApp.occursOn(nYearCompareThree,nMonthCompareThree,nDayCompareThree));
        assertTrue(dailyApp.occursOn(nYearCompareThree,nMonthCompareThree,nDayCompareThree));

    }

    /*
    Check date 2020-09-15
    should return all false
     */
    @Test
    public void testOccursOnTimeFour(){

        assertFalse(oneTimeApp.occursOn(nYearCompareFour,nMonthCompareFour,nDayCompareFour));
        assertFalse(monthlyApp.occursOn(nYearCompareFour,nMonthCompareFour,nDayCompareFour));
        assertFalse(dailyApp.occursOn(nYearCompareFour,nMonthCompareFour,nDayCompareFour));

    }


    @Test
    public void testSaveOneTime(){
        try {
            Appointment.save(strFileNameOne, oneTimeApp);
        } catch(IOException exception){
            System.out.println("File does not exist or problem with input");
        }
          /*
        read data from the file setting them into strings
        if the appointment type starts with O then set description
        and date into a Onetime object
        compare description and date from created object with
        initial Onetime object from class to check that the appointment
        was added and saved to the file
         */
        Scanner wasContentAdded = new Scanner(System.in);
        try {
            File fileCheck = new File(strFileNameOne);
            wasContentAdded = new Scanner(fileCheck);
        } catch (FileNotFoundException missingFile){
            System.out.println("file not found");
        }
        while(wasContentAdded.hasNextLine()){
            String strAppointmentType = wasContentAdded.nextLine().trim();
            String strAppointmentDescription = wasContentAdded.nextLine().trim();
            String strAppointmentDate = wasContentAdded.nextLine().trim();

            if(strAppointmentType.toUpperCase().charAt(0) == 'O'){
                Appointment oneTimeWasAdded = new Onetime(strAppointmentDescription, strAppointmentDate);
                assertEquals(oneTimeWasAdded.getAppointmentDescription(), (oneTimeApp.getAppointmentDescription()));
                assertEquals(oneTimeWasAdded.getDate(), (oneTimeApp.getDate()));
            }
        }

    }

    @Test
    public void testSaveMonthly(){
        try {
            Appointment.save(strFileNameOne, monthlyApp);
        } catch(IOException exception){
            System.out.println("File does not exist");
        }
          /*
        read data from the file setting them into strings
        if the appointment type starts with M then set description
        and date into a monthly object
        compare description and date from created object with
        initial monthly object from class to check that the appointment
        was added and saved to the file
         */
        Scanner wasContentAdded = new Scanner(System.in);
        try {
            File fileCheck = new File(strFileNameOne);
            wasContentAdded = new Scanner(fileCheck);
        } catch (FileNotFoundException f){
            System.out.println("file not found");
        }
        while(wasContentAdded.hasNextLine()){
            String strAppointmentType = wasContentAdded.nextLine().trim();
            String strAppointmentDescription = wasContentAdded.nextLine().trim();
            String strAppointmentDate = wasContentAdded.nextLine().trim();

            if(strAppointmentType.toUpperCase().charAt(0) == 'M'){
                Appointment monthlyWasAdded = new Monthly(strAppointmentDescription, strAppointmentDate);
                assertEquals(monthlyWasAdded.getAppointmentDescription(), (monthlyApp.getAppointmentDescription()));
                assertEquals(monthlyWasAdded.getDate(), (monthlyApp.getDate()));
            }
        }
    }

    @Test
    public void testSaveDaily(){
        /*
        Append daily appointment to the file
         */
        try {
            Appointment.save(strFileNameOne, dailyApp);
        } catch(IOException exception){
            System.out.println("File does not exist");
        }
        /*
        read data from the file setting them into strings
        if the appointment type starts with D then set description
        and date into a daily object
        compare description and date from created object with
        initial daily object from test class to check that the appointment was added and saved to
        the file
         */
        Scanner wasContentAdded = new Scanner(System.in);
        try {
            File fileCheck = new File(strFileNameOne);
            wasContentAdded = new Scanner(fileCheck);
        } catch (FileNotFoundException f){
            System.out.println("file not found");
        }
        while(wasContentAdded.hasNextLine()){
            String strAppointmentType = wasContentAdded.nextLine().trim();
            String strAppointmentDescription = wasContentAdded.nextLine().trim();
            String strAppointmentDate = wasContentAdded.nextLine().trim();

            if(strAppointmentType.toUpperCase().charAt(0) == 'D'){
                Appointment dailyWasAdded = new Daily(strAppointmentDescription, strAppointmentDate);
                assertEquals(dailyWasAdded.getAppointmentDescription(), (dailyApp.getAppointmentDescription()));
                assertEquals(dailyWasAdded.getDate(), (dailyApp.getDate()));
            }
        }
    }

    /*

     */
    @Test
    public void testSaveAppendedApps(){
        //create array list to store appointment objects
        ArrayList<Appointment> appointmentsChecks = new ArrayList<>();
          /*
        read data from the file setting them into strings
        if the appointment type starts with O,M, or D then set description
        and date into a  object of that type and add it to the arraylist
        compare description and date from created object with
        initial daily object from test class to check that the appointment was added and saved to
        the file
         */
        Scanner wasContentAdded = new Scanner(System.in);
        try {
            File fileCheck = new File(strFileNameOne);
            wasContentAdded = new Scanner(fileCheck);
        } catch (FileNotFoundException f){
            System.out.println("file not found");
        }
        while(wasContentAdded.hasNextLine()){
            String strAppointmentType = wasContentAdded.nextLine().trim();
            String strAppointmentDescription = wasContentAdded.nextLine().trim();
            String strAppointmentDate = wasContentAdded.nextLine().trim();

            if(strAppointmentType.toUpperCase().charAt(0) == 'D'){
                Appointment dailyWasAdded = new Onetime(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(dailyWasAdded);

            } else if(strAppointmentType.toUpperCase().charAt(0) == 'O'){
                Appointment oneTimeWasAdded = new Monthly(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(oneTimeWasAdded);

            } else if(strAppointmentType.toUpperCase().charAt(0) == 'M'){
                Appointment monthlyWasAdded = new Onetime(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(monthlyWasAdded);

            }
        }

        //store initial size of arraylist into nFileBeforeAdds
        int nFileBeforeAdds = appointmentsChecks.size();
        /*
        Append appointments of each type to the file
         */
        try {
            Appointment.save(strFileNameOne, oneTimeApp);
            Appointment.save(strFileNameOne, monthlyApp);
            Appointment.save(strFileNameOne, dailyApp);
        } catch(IOException exception){
            System.out.println("File does not exist");
        }
        /*
        read data from the file setting them into strings
        if the appointment type starts with O,M, or D then set description
        and date into a  object of that type and add it to the arraylist
        compare description and date from created object with
        initial daily object from test class to check that the appointment was added and saved to
        the file
         */
        //Scanner wasContentAdded = new Scanner(System.in);
        try {
            File fileCheck = new File(strFileNameOne);
            wasContentAdded = new Scanner(fileCheck);
        } catch (FileNotFoundException f){
            System.out.println("file not found");
        }
        while(wasContentAdded.hasNextLine()){
            String strAppointmentType = wasContentAdded.nextLine().trim();
            String strAppointmentDescription = wasContentAdded.nextLine().trim();
            String strAppointmentDate = wasContentAdded.nextLine().trim();

            if(strAppointmentType.toUpperCase().charAt(0) == 'D'){
                Appointment dailyWasAdded = new Onetime(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(dailyWasAdded);

            } else if(strAppointmentType.toUpperCase().charAt(0) == 'O'){
                Appointment oneTimeWasAdded = new Monthly(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(oneTimeWasAdded);

            } else if(strAppointmentType.toUpperCase().charAt(0) == 'M'){
                Appointment monthlyWasAdded = new Onetime(strAppointmentDescription, strAppointmentDate);
                appointmentsChecks.add(monthlyWasAdded);

            }
        }
        /*
        the size of the arraylist should be greater than it was before
        showing that the objects were appended and did not simply replace the contents
        of the file from before
        */
        assertTrue(appointmentsChecks.size() > nFileBeforeAdds);
    }

    @Test
    public void testLoadOneTimeAmount() {
        try {
            assertSame(Onetime.load(strFileNameTwo).size(), 1);
        }catch(FileNotFoundException exception){
            System.out.println("File not found here");
        }
    }

    @Test
    public void testLoadOneTimeContent() {
        try {
            assertTrue(Appointment.load(strFileNameTwo).get(0) instanceof Onetime);
        }catch(FileNotFoundException exception){
            System.out.println("File not found here");
        }
    }

    @Test
    public void testLoadMonthlyAmount(){
        try {
            assertSame(Monthly.load(strFileNameTwo).size(), 1);
        } catch(FileNotFoundException exception){
            System.out.println("File not found");
        }

    }

    @Test
    public void testLoadMonthlyContent(){
        try {
            assertTrue(Monthly.load(strFileNameTwo).get(0) instanceof Monthly);
        } catch(FileNotFoundException exception){
            System.out.println("File not found");
        }

    }

    @Test
    public void testLoadDailyAmount(){
        try {
            assertSame(Daily.load(strFileNameTwo).size(), 1);
        }catch(FileNotFoundException exception){
            System.out.println("File not found");
        }

    }

    @Test
    public void testLoadDailyContent(){
        try {
            assertTrue(Daily.load(strFileNameTwo).get(0) instanceof Daily);
        }catch(FileNotFoundException exception){
            System.out.println("File not found");
        }

    }



}
