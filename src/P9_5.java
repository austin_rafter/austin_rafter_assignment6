import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *CS 49J
 *
 * user enters A, C, L, or Q
 * if Q then end the program
 * if A then add a new appointment to a file
 * if C then check appointment times and
 * print appointments on user entered day from file
 * if L then user enters filename and appointment type
 * print out all appointments in the file of that type
 *
 * getClass and getSimpleName found at
 * https://stackoverflow.com/questions/10438448/is-there-a-way-to-output-the-java-data-type-to-the-console
 */

public class P9_5 {
    public static void main(String[] args) {

        //initiate test class constructor
        P9_5Test testMethods = new P9_5Test();

        //Test methods, program will exit if any fail
        testMethods.testOccursOnTimeOne();
        testMethods.testOccursOnTimeTwo();
        testMethods.testOccursOnTimeThree();
        testMethods.testOccursOnTimeFour();

        testMethods.testSaveOneTime();
        testMethods.testSaveMonthly();
        testMethods.testSaveDaily();

        /*
        enter O then O to pass first two
        M then M to pass second two
        D then D to pass third two
         */
           testMethods.testLoadOneTimeAmount();
        testMethods.testLoadOneTimeContent();
           testMethods.testLoadMonthlyAmount();
        testMethods.testLoadMonthlyContent();
           testMethods.testLoadDailyAmount();
        testMethods.testLoadDailyContent();
        testMethods.testSaveAppendedApps();






        /*
        User enters initial choice
         */
        System.out.print("Select an option: A for add an appointment, C for checking, L to load types of appointments, and Q to quit:");
        Scanner scanObject = new Scanner(System.in);

        String strChoiceEntered = scanObject.nextLine();

        /*
        while user hasn't entered Q then
        check if they have entered A or C
         */
        while(strChoiceEntered.toUpperCase().charAt(0) != 'Q'){

            /*
            If user entered A then check if its daily monthly or one time
            have user enter the date of appointment and description of appointment
             */
            if(strChoiceEntered.toUpperCase().charAt(0) == 'A') {

                System.out.print("Enter the filename:");
                String strFileName = scanObject.nextLine();
                File fileEntered;
                /*
                if file doesn't exist create it then alert user it was created
                otherwise let them know it exists
                if problem creating file then tell user an error happened
                 */
                try {
                     fileEntered = new File(strFileName);
                    if (fileEntered.createNewFile()) {
                        System.out.println("File created: " + fileEntered.getName());
                    } else {
                        System.out.println("File exists.");
                    }
                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }

                String strAppointmentType = " ";
                /*
                Check that user has entered a proper appointment type
                 */
                while((strAppointmentType.toUpperCase().charAt(0) != 'D') || (strAppointmentType.toUpperCase().charAt(0) != 'M') || (strAppointmentType.toUpperCase().charAt(0) != 'O')){
                    System.out.print("Enter the type O = Onetime, D = Daily, or M = Monthly:");
                    strAppointmentType = scanObject.nextLine();
                    if((strAppointmentType.toUpperCase().charAt(0) == 'D') || (strAppointmentType.toUpperCase().charAt(0) == 'M') || (strAppointmentType.toUpperCase().charAt(0) == 'O')){
                        break;
                    }
                }

                /*
                Enter initial date of appointment
                 */
                System.out.print("Enter the date (yyyy-mm-dd):");
                String strDateOfAppointment = scanObject.nextLine();

                /*
                Enter description of appointment
                 */
                System.out.print("Enter a description for the appointment: ");
                String strDescriptionOfAppointment = scanObject.nextLine();

                /*
                If user entered O then add date and description for one time object to appointments arraylist
                 */
                if (strAppointmentType.toUpperCase().charAt(0) == 'O') {

                    Appointment appointmentOneTime = new Onetime(strDescriptionOfAppointment, strDateOfAppointment);
                    try {
                        Appointment.save(strFileName, appointmentOneTime);
                    } catch(IOException exception){
                        System.out.println("File does not exist");
                    }
                }
                 /*
                If user entered M then add date and description for monthly object to appointments arraylist
                 */
                else if (strAppointmentType.toUpperCase().charAt(0) == 'M') {
                    Appointment appointmentMonthly = new Monthly(strDescriptionOfAppointment, strDateOfAppointment);
                    try {
                        Appointment.save(strFileName, appointmentMonthly);
                    } catch(IOException exception){
                        System.out.println("File does not exist");
                    }
                }
                 /*
                If user entered D then add date and description for daily object to appointments arraylist
                 */
                else if (strAppointmentType.toUpperCase().charAt(0) == 'D') {
                    Appointment appointmentDaily = new Daily(strDescriptionOfAppointment, strDateOfAppointment);
                    try {
                        Appointment.save(strFileName, appointmentDaily);
                    } catch(IOException exception){
                        System.out.println("File does not exist");
                    }
                }


            }
            /*
            If user entered C then they will enter date and
            all appointments corresponding to that date will print
            all daily appointments equal to and after the date will print
            all monthly appointments on the same day will print
             */
            else if(strChoiceEntered.toUpperCase().charAt(0) == 'C') {
                System.out.print("Enter the filename:");
                String strFileName = scanObject.nextLine();
                File fileEntered = new File(strFileName);
                /*
                if file doesn't exist have user keep entering the name until a file that does exist is found
                 */
                while(!fileEntered.exists()){
                    System.out.println("That file does not exist ");
                    System.out.print("Enter a filename:");
                    strFileName = scanObject.nextLine();
                    fileEntered = new File(strFileName);
                }


                System.out.print("Enter the date (yyyy-mm-dd):");
                String strDateOfAppointment = scanObject.nextLine();
                String[] splitToInts = strDateOfAppointment.split("-");

                int nYearCompare = Integer.parseInt(splitToInts[0]);
                int nMonthCompare = Integer.parseInt(splitToInts[1]);
                int nDayCompare = Integer.parseInt(splitToInts[2]);

                //create arraylist to copy from the method
                ArrayList<Appointment> appointmentChecks = new ArrayList<>();

                /*
                clone the array of appointments from the method to compare with user entered date
                 */
                try {
                     appointmentChecks = (ArrayList<Appointment>) Appointment.loadAllAppointments(strFileName).clone();
                } catch(FileNotFoundException exception){
                    System.out.println("File not found");
                }

                /*
                If user entered date is correct for any appointments print it
                 */
                for(int i = 0; i < appointmentChecks.size(); i++) {
                    if(appointmentChecks.get(i).occursOn(nYearCompare, nMonthCompare, nDayCompare)){
                        System.out.println("You have this appointment that day " + appointmentChecks.get(i).getAppointmentDescription());
                    }
                }
            } else if (strChoiceEntered.toUpperCase().charAt(0) == 'L') {
                System.out.print("Enter the filename:");
                String strFileName = scanObject.nextLine();
                File fileEntered = new File(strFileName);
                /*
                if file doesn't exist have user keep entering the name until a file that does exist is found
                 */
                while(!fileEntered.exists()){
                    System.out.println("That file does not exist ");
                    System.out.print("Enter a filename:");
                    strFileName = scanObject.nextLine();
                    fileEntered = new File(strFileName);
                }
                ArrayList<Appointment> appointmentTypes = new ArrayList<>();
                 /*
                clone the array of appointments from the method to compare with user entered date
                 */
                try {
                    appointmentTypes = (ArrayList<Appointment>) Appointment.load(strFileName).clone();
                } catch(FileNotFoundException exception){
                    System.out.println("File not found");
                }
                System.out.println("You wanted " + appointmentTypes.get(0).getClass().getSimpleName() + " types of appointments");

                for(int i = 0; i < appointmentTypes.size(); i++) {

                        System.out.println("You have this  " + appointmentTypes.get(i).getAppointmentDescription() + " appointment on this " + appointmentTypes.get(i).getDate() + " date that is " + appointmentTypes.get(0).getClass().getSimpleName() + " type.");

                }

            }else
            {
                System.out.println("Please enter one of the choices");
            }

            /*
            Have user choose again
             */
            System.out.print("Select an option: A for add an appointment, C for checking, L to load types of appointments, and Q to quit:");
            strChoiceEntered = scanObject.nextLine();
        }

    }

}
