
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * check if date from appointment is equal to or after the date from the user
 */

public class Daily extends Appointment {

    /*
   constructor uses super to set appointment description and date
    */
    public Daily(String strAppointmentDescription, String strDate){
        super(strAppointmentDescription, strDate);
    }


    /*
    takes date from appointment object and splits it up into ints
    checks if the ints are equal to the year, month, and day entered and return true if they are
    if they are not check if day ints are equal return true if so
    return false otherwise
     */
    public boolean occursOn(int nYear,int nMonth, int nDay){
        String strYear = Integer.toString(nYear);
        String strMonth = Integer.toString(nMonth);
        String strDay = Integer.toString(nDay);
        String strCheckDate = strYear + "-" + strMonth + "-" + strDay;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");

        String strAppointmentDate = getDate();

        LocalDate localDate = LocalDate.parse(strAppointmentDate, formatter);

        LocalDate localDateUserEntry = LocalDate.parse(strCheckDate, formatter);

        String[] splitToInts = strAppointmentDate.split("-");

        int nYearCompare = Integer.parseInt(splitToInts[0]);
        int nMonthCompare = Integer.parseInt(splitToInts[1]);
        int nDayCompare = Integer.parseInt(splitToInts[2]);
        if(localDateUserEntry.isBefore(localDate)){
            return false;
        } else if((nYear == nYearCompare) && (nMonth == nMonthCompare) && (nDay == nDayCompare)) {
            return true;
        } else if(localDateUserEntry.isAfter(localDate)) {
            return true;
        }
        return false;

    }


}
